#!/usr/bin/env python
# coding: utf-8

# ### COVID Prediction

# In[5]:


import pandas as pd
import snowflake.connector
import boto3
import json


# In[6]:


client=boto3.client('secretsmanager',region_name='ap-south-1')
response = client.get_secret_value(
    SecretId='ml_creds'
)
cred=json.loads(response['SecretString'])


# In[10]:


# Connection string
conn = snowflake.connector.connect(
                user=cred['user'],
                password=cred['password'],
                account=cred['account'],
                warehouse=cred['warehouse'],
                database=cred['database'],
                schema=cred['schema']
                )

# Create cursor
cur = conn.cursor()

# Execute SQL statement
cur.execute("select * from COVID_DATA;")

dft = cur.fetch_pandas_all()

dft=dft.drop_duplicates()
# dft.head(5)


# In[3]:



dft["DATE"]=pd.to_datetime(dft["DATE"])
dft.dropna()
dff=dft.sort_values(by='DATE',ascending=True)
# dff.head(12)


# In[4]:


df=dff.groupby("DATE").sum().reset_index()
df['DATE']


# In[5]:


df.size


# In[6]:


# df.tail()


# In[7]:


# c=df[df.State_id==2]
# df=pd.DataFrame(c[["Date","Cases"]])
# df.set_index(['Date'],inplace=True)
# df.plot()


# In[8]:


cases=df.reset_index()['CONFIRMED']
deaths=df.reset_index()['DECEASED']
recovered=df.reset_index()['RECOVERED']
# recovered.plot()
# cases.plot()


# In[11]:


# print(cases,
# deaths,
# recovered)


# In[10]:


import matplotlib.pyplot as plt
# plt.plot(cases,c='blue')
# plt.plot(recovered,c='green')


# In[11]:


# plt.plot(deaths,c='yellow')


# In[12]:


### LSTM are sensitive to the scale of the data. so we apply MinMax scaler 


# In[13]:


import numpy as np


# In[14]:


from sklearn.preprocessing import MinMaxScaler
cases_scaler=MinMaxScaler(feature_range=(0,1))
deaths_scaler=MinMaxScaler(feature_range=(0,1))
recovered_scaler=MinMaxScaler(feature_range=(0,1))
cases=cases_scaler.fit_transform(np.array(cases).reshape(-1,1))
deaths=deaths_scaler.fit_transform(np.array(deaths).reshape(-1,1))
recovered=recovered_scaler.fit_transform(np.array(recovered).reshape(-1,1))


# In[15]:


print(cases, deaths,recovered)


# In[16]:


##splitting dataset into train and test split
training_size=int(len(cases)*0.65)
cases_test_size=len(cases)-training_size
deaths_test_size=len(deaths)-training_size
recovered_test_size=len(recovered)-training_size
cases_train_data,cases_test_data=cases[0:training_size,:],cases[training_size:len(cases),:1]
deaths_train_data,deaths_test_data=deaths[0:training_size,:],deaths[training_size:len(deaths),:1]
recovered_train_data,recovered_test_data=recovered[0:training_size,:],recovered[training_size:len(recovered),:1]


# In[17]:


training_size,cases_test_size,deaths_test_size,recovered_test_size


# In[18]:


cases_train_data


# In[19]:


import numpy
# convert an array of values into a dataset matrix
def create_dataset(dataset, time_step=1):
	dataX, dataY = [], []
	for i in range(len(dataset)-time_step-1):
		a = dataset[i:(i+time_step), 0]   ###i=0, 0,1,2,3-----99   100 
		dataX.append(a)
		dataY.append(dataset[i + time_step, 0])
	return numpy.array(dataX), numpy.array(dataY)


# In[20]:


# reshape into X=t,t+1,t+2,t+3 and Y=t+4
time_step = 90
cases_X_train, cases_y_train = create_dataset(cases_train_data, time_step)
cases_X_test, cases_ytest = create_dataset(cases_test_data, time_step)

deaths_X_train, deaths_y_train = create_dataset(deaths_train_data, time_step)
deaths_X_test, deaths_ytest = create_dataset(deaths_test_data, time_step)

recovered_X_train, recovered_y_train = create_dataset(recovered_train_data, time_step)
recovered_X_test, recovered_ytest = create_dataset(recovered_test_data, time_step)


# In[21]:


print(cases_X_train.shape), print(cases_y_train.shape)


# In[22]:


print(cases_X_test.shape), print(cases_ytest.shape)


# In[23]:


# reshape input to be [samples, time steps, features] which is required for LSTM
#cases
cases_X_train =cases_X_train.reshape(cases_X_train.shape[0],cases_X_train.shape[1] , 1)
cases_X_test = cases_X_test.reshape(cases_X_test.shape[0],cases_X_test.shape[1] , 1)
# Deaths
deaths_X_train =deaths_X_train.reshape(deaths_X_train.shape[0],deaths_X_train.shape[1] , 1)
deaths_X_test = deaths_X_test.reshape(deaths_X_test.shape[0],deaths_X_test.shape[1] , 1)
# recovered
recovered_X_train =recovered_X_train.reshape(recovered_X_train.shape[0],recovered_X_train.shape[1] , 1)
recovered_X_test = recovered_X_test.reshape(recovered_X_test.shape[0],recovered_X_test.shape[1] , 1)


# In[24]:


# pip install tensorflow


# In[25]:


### Create the Stacked LSTM model
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import LSTM


# In[26]:


model=Sequential()
model.add(LSTM(50,return_sequences=True,input_shape=(90,1)))
model.add(LSTM(50,return_sequences=True))
model.add(LSTM(50))
model.add(Dense(1))
model.compile(loss='mean_squared_error',optimizer='adam')


# In[27]:


model.summary()


# In[ ]:





# In[28]:


model.fit(cases_X_train,cases_y_train,validation_data=(cases_X_test,cases_ytest),epochs=100,batch_size=64,verbose=1)


# In[29]:


import tensorflow as tf


# In[30]:


tf.__version__


# In[31]:


### Lets Do the prediction and check performance metrics
cases_train_predict=model.predict(cases_X_train)
cases_test_predict=model.predict(cases_X_test)


# In[32]:


##Transformback to original form
cases_train_predict=cases_scaler.inverse_transform(cases_train_predict)
cases_test_predict=cases_scaler.inverse_transform(cases_test_predict)


# In[33]:


### Calculate RMSE performance metrics
import math
from sklearn.metrics import mean_squared_error
math.sqrt(mean_squared_error(cases_y_train,cases_train_predict))


# In[34]:


### Test Data RMSE
math.sqrt(mean_squared_error(cases_ytest,cases_test_predict))


# In[35]:


### Plotting 
# shift train predictions for plotting
look_back=90
trainPredictPlot = numpy.empty_like(cases)
trainPredictPlot[:, :] = np.nan
trainPredictPlot[look_back:len(cases_train_predict)+look_back, :] = cases_train_predict
# shift test predictions for plotting
testPredictPlot = numpy.empty_like(cases)
testPredictPlot[:, :] = numpy.nan
testPredictPlot[len(cases_train_predict)+(look_back*2)+1:len(cases)-1, :] = cases_test_predict
# plot baseline and predictions
# plt.plot(cases_scaler.inverse_transform(cases))
# plt.plot(trainPredictPlot)
# plt.plot(testPredictPlot)
# plt.show()


# In[36]:


len(cases_test_data)


# In[37]:


x_input=cases_test_data[159:].reshape(1,-1)
x_input.shape


# In[ ]:





# In[ ]:





# In[38]:


temp_input=list(x_input)
temp_input=temp_input[0].tolist()


# In[39]:


temp_input


# In[40]:


# demonstrate prediction for next 10 days
from numpy import array

lst_output=[]
n_steps=90
i=0
while(i<90):
    
    if(len(temp_input)>90):
        #print(temp_input)
        x_input=np.array(temp_input[1:])
        print("{} day input {}".format(i,x_input))
        x_input=x_input.reshape(1,-1)
        x_input = x_input.reshape((1, n_steps, 1))
        #print(x_input)
        yhat = model.predict(x_input, verbose=0)
        print("{} day output {}".format(i,yhat))
        temp_input.extend(yhat[0].tolist())
        temp_input=temp_input[1:]
        #print(temp_input)
        lst_output.extend(yhat.tolist())
        i=i+1
    else:
        x_input = x_input.reshape((1, n_steps,1))
        yhat = model.predict(x_input, verbose=0)
        print(yhat[0])
        temp_input.extend(yhat[0].tolist())
        print(len(temp_input))
        lst_output.extend(yhat.tolist())
        i=i+1
    

print(lst_output)


# In[41]:


day_new=np.arange(1,91)
day_pred=np.arange(91,271)


# In[42]:


import matplotlib.pyplot as plt


# In[43]:


len(cases)


# In[ ]:





# In[44]:


# plt.plot(day_new,cases_scaler.inverse_transform(cases[622:]))
# plt.plot(day_pred,cases_scaler.inverse_transform(lst_output))
# plt.legend(labels=["Trained Cases","Predicted list Cases"],loc='upper right')


# In[45]:


df3=cases.tolist()
df3.extend(lst_output)
# plt.plot(df3)


# In[46]:


df3=cases_scaler.inverse_transform(df3).tolist()
len(df3)-len(cases)
x=[]
df=dff.groupby("DATE").sum().reset_index()
from datetime import *
for i in range(1,181):
    x.append(str(pd.to_datetime(date.today()+timedelta(days=i)).isoformat()).replace('T',' '))
x
x=df.append(pd.DataFrame({'DATE':x}))
# x
df3


# In[47]:


# plt.plot(x['DATE'],df3,c='red')
# plt.plot(df['DATE'],df3[:712],c='blue')
# plt.legend(labels=["Predicted Cases","Actual Cases"],loc='upper left')


# In[48]:


# plt.plot(x['DATE'],df3)


# In[49]:


# plt.plot(cases)


# In[50]:


# plt.plot(df3,c='red')
# plt.plot(cases,c='blue')


# In[51]:


model.fit(deaths_X_train,deaths_y_train,validation_data=(deaths_X_test,deaths_ytest),epochs=100,batch_size=64,verbose=1)

### Lets Do the prediction and check performance metrics
deaths_train_predict=model.predict(deaths_X_train)
deaths_test_predict=model.predict(deaths_X_test)

##Transformback to original form
deaths_train_predict=deaths_scaler.inverse_transform(deaths_train_predict)
deaths_test_predict=deaths_scaler.inverse_transform(deaths_test_predict)


# In[52]:


### Calculate RMSE performance metrics
import math
from sklearn.metrics import mean_squared_error
math.sqrt(mean_squared_error(deaths_y_train,deaths_train_predict))

### Test Data RMSE
math.sqrt(mean_squared_error(deaths_ytest,deaths_test_predict))

### Plotting 
# shift train predictions for plotting
look_back=90
trainPredictPlot = numpy.empty_like(deaths)
trainPredictPlot[:, :] = np.nan
trainPredictPlot[look_back:len(deaths_train_predict)+look_back, :] = deaths_train_predict
# shift test predictions for plotting
testPredictPlot = numpy.empty_like(deaths)
testPredictPlot[:, :] = numpy.nan
testPredictPlot[len(deaths_train_predict)+(look_back*2)+1:len(deaths)-1, :] = deaths_test_predict
# plot baseline and predictions
# plt.plot(deaths_scaler.inverse_transform(deaths))
# plt.plot(trainPredictPlot)
# plt.plot(testPredictPlot)
# plt.show()

len(deaths_test_data)

x_input=deaths_test_data[159:].reshape(1,-1)
x_input.shape


temp_input=list(x_input)
temp_input=temp_input[0].tolist()

temp_input

# demonstrate prediction for next 10 days
from numpy import array

lst_output=[]
n_steps=90
i=0
while(i<90):
    
    if(len(temp_input)>90):
        #print(temp_input)
        x_input=np.array(temp_input[1:])
        print("{} day input {}".format(i,x_input))
        x_input=x_input.reshape(1,-1)
        x_input = x_input.reshape((1, n_steps, 1))
        #print(x_input)
        yhat = model.predict(x_input, verbose=0)
        print("{} day output {}".format(i,yhat))
        temp_input.extend(yhat[0].tolist())
        temp_input=temp_input[1:]
        #print(temp_input)
        lst_output.extend(yhat.tolist())
        i=i+1
    else:
        x_input = x_input.reshape((1, n_steps,1))
        yhat = model.predict(x_input, verbose=0)
        print(yhat[0])
        temp_input.extend(yhat[0].tolist())
        print(len(temp_input))
        lst_output.extend(yhat.tolist())
        i=i+1
    

print(lst_output)

day_new=np.arange(1,91)
day_pred=np.arange(91,271)

import matplotlib.pyplot as plt


# In[53]:


len(deaths)


# In[54]:


# plt.plot(day_new,deaths_scaler.inverse_transform(deaths[622:]))
# plt.plot(day_pred,deaths_scaler.inverse_transform(lst_output))

df4=deaths.tolist()
df4.extend(lst_output)
# plt.plot(df4)


# In[55]:


df4=deaths_scaler.inverse_transform(df4).tolist()
# len(df4)-len(deaths)
# plt.plot(x['DATE'],df4,c='red')
# plt.plot(df['DATE'],df4[:712],c='blue')
# plt.legend(labels=["Actual deaths","Predicted Deaths"])


# In[56]:


# plt.plot(recovered)


# In[57]:


model.fit(recovered_X_train,recovered_y_train,validation_data=(recovered_X_test,recovered_ytest),epochs=100,batch_size=100,verbose=1)


# In[58]:


import tensorflow as tf


# In[59]:


tf.__version__


# In[60]:


### Lets Do the prediction and check performance metrics
recovered_train_predict=model.predict(recovered_X_train)
recovered_test_predict=model.predict(recovered_X_test)


# In[61]:


##Transformback to original form
recovered_train_predict=recovered_scaler.inverse_transform(recovered_train_predict)
recovered_test_predict=recovered_scaler.inverse_transform(recovered_test_predict)


# In[62]:


### Calculate RMSE performance metrics
import math
from sklearn.metrics import mean_squared_error
math.sqrt(mean_squared_error(recovered_y_train,recovered_train_predict))


# In[63]:


### Test Data RMSE
math.sqrt(mean_squared_error(recovered_ytest,recovered_test_predict))


# In[64]:


### Plotting 
# shift train predictions for plotting
look_back=90
trainPredictPlot = numpy.empty_like(recovered)
trainPredictPlot[:, :] = np.nan
trainPredictPlot[look_back:len(recovered_train_predict)+look_back, :] = recovered_train_predict
# shift test predictions for plotting
testPredictPlot = numpy.empty_like(recovered)
testPredictPlot[:, :] = numpy.nan
testPredictPlot[len(recovered_train_predict)+(look_back*2)+1:len(recovered)-1, :] = recovered_test_predict
# plot baseline and predictions
# plt.plot(recovered_scaler.inverse_transform(recovered))
# plt.plot(trainPredictPlot)
# plt.plot(testPredictPlot)
# plt.show()


# In[65]:


len(recovered_test_data)


# In[66]:


x_input=recovered_test_data[160:].reshape(1,-1)
x_input.shape


# In[ ]:





# In[ ]:





# In[67]:


temp_input=list(x_input)
temp_input=temp_input[0].tolist()


# In[68]:


temp_input


# In[69]:


# demonstrate prediction for next 10 days
from numpy import array

lst_output=[]
n_steps=90
i=0
while(i<90):
    
    if(len(temp_input)>90):
        #print(temp_input)
        x_input=np.array(temp_input[1:])
        print("{} day input {}".format(i,x_input))
        x_input=x_input.reshape(1,-1)
        x_input = x_input.reshape((1, n_steps, 1))
        #print(x_input)
        yhat = model.predict(x_input, verbose=0)
        print("{} day output {}".format(i,yhat))
        temp_input.extend(yhat[0].tolist())
        temp_input=temp_input[1:]
        #print(temp_input)
        lst_output.extend(yhat.tolist())
        i=i+1
    else:
        x_input = x_input.reshape((1, n_steps,1))
        yhat = model.predict(x_input, verbose=0)
        print(yhat[0])
        temp_input.extend(yhat[0].tolist())
        print(len(temp_input))
        lst_output.extend(yhat.tolist())
        i=i+1
    

print(lst_output)


# In[70]:


day_new=np.arange(1,91)
day_pred=np.arange(91,271)


# In[71]:


import matplotlib.pyplot as plt


# In[72]:


len(recovered)


# In[ ]:





# In[73]:


# plt.plot(day_new,recovered_scaler.inverse_transform(recovered[622:]))
# plt.plot(day_pred,recovered_scaler.inverse_transform(lst_output))
# plt.legend(labels=["Predicted Cases","Actual Cases"],loc='upper right')


# In[74]:


df5=cases.tolist()
df5.extend(lst_output)
# plt.plot(df5)


# In[75]:


df5=recovered_scaler.inverse_transform(df5).tolist()
len(df5)-len(recovered)
df5[:10]


# In[76]:


# plt.plot(recovered,c='red')


# In[77]:


# plt.plot(df5)


# In[78]:


# plt.plot(df5)
recovery=df5[:712]


# In[79]:


# plt.plot(x['DATE'],df5,c='red')
# plt.plot(df['DATE'],df5[:712],c='blue')
# plt.legend(labels=["Predicted Recovery","Actual Recovery"],loc='upper left')


df3=[''.join(map(str, l)) for l in df3]
case=pd.DataFrame({'CASES':df3})
case.index=x["DATE"]
case.to_csv("Cases.csv")

df4=[''.join(map(str, l)) for l in df4]
death=pd.DataFrame({'DECEASED':df4})
death.index=x["DATE"]
death.to_csv("Deaths.csv")

df5=[''.join(map(str, l)) for l in df5]
recover=pd.DataFrame({'RECOVERED':df5})
recover.index=x["DATE"]
recover.to_csv("Recovered.csv")


# In[80]:





# In[8]:


s3 = boto3.resource(
    service_name='s3'
)


# In[9]:


for bucket in s3.buckets.all():
    print(bucket.name)

s3.Bucket('ml-covid-prediction').upload_file(Filename='Cases.csv', Key='COVID_Prediction/Cases.csv')
s3.Bucket('ml-covid-prediction').upload_file(Filename='Deaths.csv', Key='COVID_Prediction/Deaths.csv')
s3.Bucket('ml-covid-prediction').upload_file(Filename='Recovered.csv', Key='COVID_Prediction/Recovered.csv')

